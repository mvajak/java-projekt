package com.example.mihkelkristofervajak.todolist;

/**
 * @author Mihkel Kristofer Vajak
 */
public class ToDoList {

    private String _title, _description;
    private int _id;

    public ToDoList(int id,String title, String description) {

        _description = description;
        _title = title;
        _id = id;
    }

    public int getId() {return _id; }

    /**
     * Sisestatud Title'i getter
     * @return
     */
    public String getTitle() {
        return _title;
    }

    /**
     * Sisestatud descriptioni getter
     * @return
     */
    public String getDescription() {
        return _description;
    }
}