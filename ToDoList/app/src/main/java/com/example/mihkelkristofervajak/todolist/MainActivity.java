package com.example.mihkelkristofervajak.todolist;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.*;

/**
 * MainActivity juhib kogu rakendust.
 * @author Mihkel Kristofer Vajak
 */

public class MainActivity extends AppCompatActivity {
    /**
     * Muutujad mida kasutame longclick variandi puhul
     */
    private static final int DELETE = 0;

    EditText titleTxt, descriptiontxt;
    List<ToDoList> itemList = new ArrayList<ToDoList>();
    ListView itemListView;
    DatabaseHandler dbHandler;
    int longClickedItemIndex;
    ArrayAdapter<ToDoList> itemAdapter;

    /**
     * See meetod võimaldab kastisse panna linnukese, kui mingi tegevus listist on tehtud.
     * @param view
     */
    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch(view.getId()) {
            case R.id.checkBox1:
                if (checked) {
                }else {
                    break;
                }case R.id.checkBox2:
                if (checked) {
                }else {
                    break;
                }case R.id.checkBox3:
                if (checked) {
                }else {
                    break;
                }case R.id.checkBox4:
                if (checked) {
                }else {
                    break;
                }
        }
    }

    /**
     * See meetod käivitub koheselt, kui rakendus on tööle pandud
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * Siin tehakse kindlaks muutujad, ehk antakse id, mis määrati eelnevalt content_main.xml-is
         */
        titleTxt = (EditText) findViewById(R.id.txtTitle);
        descriptiontxt = (EditText) findViewById(R.id.txtDescription);
        itemListView = (ListView) findViewById(R.id.listView);
        dbHandler = new DatabaseHandler(getApplicationContext());

        registerForContextMenu(itemListView);

        /**
         * Juhul kui hoitakse sõrme itemi peal
         */
        itemListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                longClickedItemIndex = position;
                return false;
            }
        });

        /**
         * Siit edasi OnCreate meetod käima 2 tabi , mis kuvatakse rakenduses üleval pool, Creator ja List.
         */

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("creator");
        tabSpec.setContent(R.id.tabCreator);
        tabSpec.setIndicator("Creator");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("list");
        tabSpec.setContent(R.id.tabList);
        tabSpec.setIndicator("List");
        tabHost.addTab(tabSpec);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.sound);

        final Button addBtn = (Button) this.findViewById(R.id.btnAdd);
        addBtn.setOnClickListener(new View.OnClickListener() {
            /**
             * Selles meetodis kirjeldatakse, mis juhtub, kui vajutada nuppu Add Item
             * @param v
             */
            @Override
            public void onClick(View v) {
                mp.start();
                ToDoList item = new ToDoList(dbHandler.getItemsCount(), String.valueOf(titleTxt.getText()), String.valueOf(descriptiontxt.getText()));
                dbHandler.createList(item);
                itemList.add(item);
                itemAdapter.notifyDataSetChanged();
                populateList();
                Toast.makeText(getApplicationContext(), titleTxt.getText().toString() + " has been added to your todo list!", Toast.LENGTH_SHORT).show();
            }
        });
        /**
         * Siin meetodites teostatakse title'i ja description'i edastamist nupule vajutades. Kui lahter on tühi, ei saa Add Item nuppu vajutada
         */
        titleTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                addBtn.setEnabled(!titleTxt.getText().toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        //automatically generated
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        /**
         * Lisab database'i andmed
         */
        List<ToDoList> addableItems = dbHandler.getAllItems();
        int itemCount = dbHandler.getItemsCount();

        for (int i = 0; i < itemCount; i++) {
            itemList.add(addableItems.get(i));
        }

        if (!addableItems.isEmpty()){
            populateList();
        }
    }

    /**
     * Moodustame menüü, mis juhtub kui all hoida üht itemit
     * @param menu
     * @param view
     * @param menuInfo
     */
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, view, menuInfo);

        menu.setHeaderIcon(R.drawable.pencil_icon);
        menu.setHeaderTitle("Item options");
        menu.add(Menu.NONE, DELETE, menu.NONE, "Delete item");
    }

    /**
     * All hoidmise tekiva menüü tegevused
     * @param item
     * @return
     */
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case DELETE:
                dbHandler.deleteItem(itemList.get(longClickedItemIndex));
                itemList.remove(longClickedItemIndex);
                itemAdapter.notifyDataSetChanged();
                break;
        }

        return super.onContextItemSelected(item);
    }

    /**
     *See meetod on selleks, et kui iga kord vajutada Add Item nuppu, peab selle info edastama List tabi.
     */
    private void populateList() {
        itemAdapter = new ToDoListAdapter();
        itemListView.setAdapter(itemAdapter);
    }

    /**
     * See meetod teeb uue adapteri, mida saaks kasutada populateList meetodis
     */
    private class ToDoListAdapter extends ArrayAdapter<ToDoList> {
        public ToDoListAdapter() {
            super(MainActivity.this, R.layout.listview_item, itemList);
        }

        /**
         * See meetod kujutab endast funktiooni, mis tagastab kogu massiivi info ühe elemendina.
         * Elemendis on kogu info (title ja description "kokku pakitud")
         * @param position
         * @param view
         * @param parent
         * @return
         */
        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null)
                view = getLayoutInflater().inflate(R.layout.listview_item, parent, false);

            ToDoList currentToDoList = itemList.get(position);

            TextView title = (TextView) view.findViewById(R.id.titleName);
            title.setText(currentToDoList.getTitle());

            TextView description = (TextView) view.findViewById(R.id.descriptionName);
            description.setText(currentToDoList.getDescription());

            return view;
        }
    }
    //automatically generated
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    //automatically generated
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}