package com.example.mihkelkristofervajak.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mihkelkristofervajak on 17/12/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    /**
     * Muutujad
     */

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_TITLE = "todolist",
    TABLE_ITEMSTODO = "itemstodo",
    KEY_ID = "id",
    KEY_TITLE = "title",
    KEY_DESCRIPTION = "description";

    /**
     * See meetod laseb SQLil töötada nende muutujatega. DATABASE_TITLE loob faili ja DATABASE_VERISON võimaldab uuendada database'i automaatselt
     * @param context
     */
    public DatabaseHandler(Context context) {
        super(context, DATABASE_TITLE, null, DATABASE_VERSION);

    }

    /**
     * Kui database on loodud, siis käivitub automaatselt see meetod
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_ITEMSTODO + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " TEXT," + KEY_DESCRIPTION + " TEXT)");
    }

    /**
     * Iga kord kui database'is toimuvad muutused, siis see meetod uuendab
     */

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMSTODO);

        onCreate(db);
    }

    /**
     * Moodustab database'i listi. Väärtused on title ja description, lõpus lisab listi
     * @param item
     */
    public void createList(ToDoList item) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, item.getTitle());
        values.put(KEY_DESCRIPTION, item.getDescription());

        db.insert(TABLE_ITEMSTODO, null, values);
        db.close();
    }

    /**
     *See meetod on items'ite lugemiseks (sellepärast ka getReadableDatabase()). Tagastab samuti itemi
     * @param id
     * @return
     */
    public ToDoList getItem(int id){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_ITEMSTODO, new String[] {KEY_ID, KEY_TITLE, KEY_DESCRIPTION}, KEY_ID + "=?", new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        ToDoList item = new ToDoList(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
        db.close();
        cursor.close();
        return item;
    }

    /**
     * Võimaldab database'ist kustuda andmeid
     * @param item
     */
    public void deleteItem(ToDoList item) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_ITEMSTODO, KEY_ID + "=?", new String[] { String.valueOf(item.getId()) });
        db.close();
    }

    /**
     * Loeb üle palju on item'eid lists ja tagastab numbrilise väärtuse
     * @return
     */
    public int getItemsCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ITEMSTODO, null);
        int count  = cursor.getCount();
        db.close();
        cursor.close();
        return count;
    }

    /**
     * Uuendab itemit. Loeb mis on database'is ja võimaldad selle sisu muuta.
     * @param item
     * @return
     */
    public int updateItem (ToDoList item) {
        SQLiteDatabase db = getReadableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, item.getTitle());
        values.put(KEY_DESCRIPTION, item.getDescription());

        return db.update(TABLE_ITEMSTODO, values, KEY_ID + "=?", new String[]{String.valueOf(item.getId())});
    }

    /**
     * Võtab terve listi, mis on database'is
     * @return
     */
    public List<ToDoList> getAllItems() {
        List<ToDoList> items = new ArrayList<ToDoList>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ITEMSTODO, null);

        /**
         * Juhtub siis, kui näiteks pole mingit TABEL_ITEMSTODO'd
         */
        if(cursor.moveToFirst()){
            do {
                ToDoList item = new ToDoList(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2));
                items.add(item);
            }
            while (cursor.moveToNext());
        }
        return items;
    }
}